import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";

import character from "../slice/character.slice";

const rootReducer = combineReducers({
  character,
});

const store = configureStore({ reducer: rootReducer });

export default store;

import { createSlice } from "@reduxjs/toolkit";
import Axios from "axios";

export const STATUS_LOADING = "status_loading";
export const STATUS_LOADED = "status_loaded";
export const STATUS_ERROR = "status_error";

const initialState = {
  page: 1,
  status: STATUS_LOADED,
  error: "",
  character: [],
  baseCharacter: [],
};

const characterSlice = createSlice({
  name: "character",
  initialState: initialState,
  reducers: {
    storeCharacter(state, action) {
      const { data } = action.payload;
      state.status = STATUS_LOADED;
      state.error = "";
      state.character = data;
      state.baseCharacter = data;
    },
    loading(state, action) {
      state.status = STATUS_LOADING;
      state.error = "";
    },
    storeError(state, action) {
      const { error = "Error" } = action.payload;
      state.status = STATUS_ERROR;
      state.error = error;
    },
    searchCharacter(state, action) {
      const { filter } = action.payload;
      console.log(filter);
      state.character = state.baseCharacter.filter((ele) => {
        return ele.name.includes(filter);
      });
    },
    deleteCharacter(state, action) {
      const { index } = action.payload;
      state.baseCharacter.splice(index, 1);
      state.character.splice(index, 1);
    },
  },
});

export const {
  loading,
  storeError,
  storeCharacter,
  deleteCharacter,
  searchCharacter,
} = characterSlice.actions;

export const fetchCharacter = () => async (dispatch, getStatus) => {
  const characterStatus = getStatus();

  if (characterStatus.status === STATUS_LOADING) {
    return;
  }

  dispatch(loading());

  Axios.get("https://swapi.dev/api/people/", {
    params: {
      page: 1,
    },
  })
    .then((response) => {
      dispatch(
        storeCharacter({
          data: response.data.results.map(({ name, height, gender }) => ({
            name,
            height,
            gender,
          })),
        })
      );
    })
    .catch((error) => {
      dispatch(storeError({ error: error.message }));
    });
};

export const currentCharacters = (characterState) => {
  const { character } = characterState;
  return character || {};
};

export default characterSlice.reducer;

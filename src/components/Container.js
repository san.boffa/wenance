import React from "react";
import styled from "styled-components";

const StyledContainer = styled.div`
  top: 0;
  position: absolute;
  width: 100%;
  padding-top: 80px;
  background-color: #f2f2f2;
  min-height: 100vh;
  padding-bottom: 50px;

  @media (max-width: 720px) {
    padding-top: 100px;
  }

  @media (max-width: 350px) {
    padding-top: 150px;
  }
`;

const Container = (props) => {
  return <StyledContainer {...props}>{props.children}</StyledContainer>;
};

export default Container;

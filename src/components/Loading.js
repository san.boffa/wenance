import React from "react";
import styled from "styled-components";

const StyledPage = styled.div`
  display: flex;
  height: 500px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: #fff;
  border-radius: 42px;
`;

const StyledLoading = styled.div`
  margin: 0 auto;
  border: 16px solid #f3f3f3;
  border-top: 16px solid #3498db;
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const Loading = () => {
  return (
    <StyledPage>
      <h1>Loading...</h1>
      <StyledLoading />
    </StyledPage>
  );
};

export default Loading;

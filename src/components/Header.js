import React from "react";
import styled, { css } from "styled-components";

const StyledHeader = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 12vw;
  background-color: #ffffff;

  @media (max-width: 720px) {
    height: 200px;
  }
`;

const StyledHeaderTitle = styled.h1`
  margin-left: 5%;
  font-style: normal;
  font-weight: bold;
  font-size: 3vw;

  @media (max-width: 720px) {
    font-size: 30px;
  }
`;

const Header = (props) => {
  return (
    <StyledHeader {...props}>
      <StyledHeaderTitle>{props.title}</StyledHeaderTitle>
    </StyledHeader>
  );
};

export default Header;

import React, { useContext } from "react";
import styled from "styled-components";

const StyledUserInfo = styled.div`
  display: flex;
  margin-right: 42px;
  color: #ffffff;

  @media (max-width: 720px) {
    margin: 0;
  }
`;

const StyledUserName = styled.div`
  margin: auto;
  font-size: 20px;
  line-height: 48px;
`;

const StyledUserLogo = styled.div`
  background-color: #4bab52;
  color: #ffffff;
  height: 35px;
  width: 35px;
  border-radius: 26px;
  margin: auto;
  line-height: 35px;
  margin: 10px;
  font-size: 21px;
`;

const UserInfo = () => {
  return (
    <StyledUserInfo>
      <StyledUserName>Usuario Uno</StyledUserName>
      <StyledUserLogo>U</StyledUserLogo>
    </StyledUserInfo>
  );
};

export default UserInfo;

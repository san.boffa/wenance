import React from "react";
import styled from "styled-components";

const StyledPage = styled.div`
  display: flex;
  height: 500px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: #fff;
  border-radius: 42px;
`;

const Error = ({ error }) => {
  return (
    <StyledPage>
      <h1>Oooooops !!</h1>
      <h2>An error occurred</h2>
      <h3>{error}</h3>
    </StyledPage>
  );
};

export default Error;

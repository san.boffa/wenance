import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { connect } from "react-redux";
import { deleteCharacter } from "../slice/character.slice";
import { GiBodyHeight } from "react-icons/gi";
import { IoTransgenderSharp } from "react-icons/io5";

const StyledCardContainer = styled.div`
  height: 250px;
  width: 100%;
  border-radius: 20px;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
  background-color: #ffffff;

  @media (max-width: 350px) {
    height: 300px;
  }
`;

const StyledCard = styled.div`
  display: flex;
  flex-direction: column;
  margin: 20px;
`;

const StyledCardTitle = styled.h1`
  margin-top: 0;
  font-size: 2rem;
  margin-bottom: 0;
`;

const StyledCharacterInfo = styled.div`
  text-align: inherit;
  width: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const StyledContainerInfo = styled.div`
  display: flex;
  height: 130px;
`;

const StyledField = styled.span`
  font-size: 22px;
  font-weight: 700;
`;

const StyledValue = styled.div`
  display: flex;
  align-items: center;
  font-size: 22px;
  text-transform: capitalize;
  margin-top: 20px;
`;

const StyledButtonDelete = styled.button`
  height: 43px;
  width: fit-content;
  padding: 0px 20px;
  align-self: flex-end;
  font-size: 16px;
  color: #000000;
  background-color: #ffffff;
  border-radius: 13px;
  border: 1px solid #dadada;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  &:hover {
    outline: none;
    border: 1px solid rgb(157, 157, 157);
    background-color: red;
    color: #ffffff;
  }
`;

const CardComponent = ({ deleteCharacter, character, index }) => {
  return (
    <StyledCardContainer>
      <StyledCard>
        <StyledCardTitle>{character.name}</StyledCardTitle>
        <StyledContainerInfo>
          <StyledCharacterInfo>
            <StyledField>Height</StyledField>
            <StyledValue>
              {character.height}
              <GiBodyHeight
                style={{
                  marginLeft: "11px",
                  fontSize: "30px",
                  color: "#5aa3d0",
                }}
              />
            </StyledValue>
          </StyledCharacterInfo>
          <StyledCharacterInfo>
            <StyledField>Gender</StyledField>
            <StyledValue>
              {character.gender}
              <IoTransgenderSharp
                style={{
                  marginLeft: "11px",
                  fontSize: "30px",
                  color: "rgb(112, 76, 182)",
                }}
              />
            </StyledValue>
          </StyledCharacterInfo>
        </StyledContainerInfo>
        <StyledButtonDelete onClick={() => deleteCharacter({ index })}>
          Eliminar
        </StyledButtonDelete>
      </StyledCard>
    </StyledCardContainer>
  );
};

CardComponent.propTypes = {
  deleteCharacter: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  status: state.character.status,
  error: state.character.error,
});

const mapDispatchToProps = {
  deleteCharacter: deleteCharacter,
};

// Connect basic component to Redux store
const Card = connect(mapStateToProps, mapDispatchToProps)(CardComponent);

export default Card;

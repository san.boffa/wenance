import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";
import { searchCharacter } from "../slice/character.slice";

const StyledSearchForm = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;

  @media (max-width: 450px) {
    flex-direction: column;
    margin-bottom: 15px;
  }
`;

const StyledInputSearch = styled.input`
  padding: 0px 20px;
  height: 43px;
  margin: 20px;
  font-size: 16px;
  color: #767676;
  background-color: #ffffff;
  border-radius: 13px;
  border: 1px solid #ffffff;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
  &:focus {
    outline: none;
    border: 1px solid rgb(157, 157, 157);
  }
`;

const StyledButtonSearch = styled.button`
  cursor: pointer;
  padding: 0px 20px;
  height: 43px;
  font-size: 16px;
  color: #000000;
  background-color: #ffffff;
  border-radius: 13px;
  border: 1px solid #ffffff;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
  &:hover {
    outline: none;
    border: 1px solid rgb(157, 157, 157);
  }
`;

const SearchFormComponent = ({ searchCharacter }) => {
  const [searchValue, setSearchValue] = useState("");

  const handleClickSearch = () => {
    searchCharacter({ filter: searchValue });
  };

  return (
    <StyledSearchForm>
      <StyledInputSearch
        onChange={(e) => setSearchValue(e.target.value)}
        type="text"
        placeholder="Search Character..."
      />
      <StyledButtonSearch onClick={handleClickSearch}>
        Search
      </StyledButtonSearch>
    </StyledSearchForm>
  );
};

SearchFormComponent.propTypes = {
  searchCharacter: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  status: state.character.status,
});

const mapDispatchToProps = {
  searchCharacter: searchCharacter,
};

const SearchForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchFormComponent);

export default SearchForm;

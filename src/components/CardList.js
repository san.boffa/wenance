import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { connect } from "react-redux";
import Card from "./Card";
import Error from "./Error";
import Loading from "./Loading";
import NoResult from "./NoResult";
import {
  currentCharacters,
  STATUS_LOADING,
  STATUS_LOADED,
  STATUS_ERROR,
} from "../slice/character.slice";

const StyledCardList = styled.div`
  display: grid;
  margin: 0;
  padding: 0;
  justify-content: center;
  align-content: center;
  grid-template-columns: repeat(auto-fill, 400px);
  grid-gap: 24px;

  @media (max-width: 930px) {
    grid-template-columns: repeat(auto-fill, 100%);
  }
`;

const CardListComponent = ({ currentCharacters, status, error }) => {
  return status === STATUS_LOADED ? (
    currentCharacters.length > 0 ? (
      <StyledCardList>
        {currentCharacters.map((character, index) => {
          return <Card character={character} key={index} index={index} />;
        })}
      </StyledCardList>
    ) : (
      <NoResult />
    )
  ) : status === STATUS_LOADING ? (
    <Loading />
  ) : status === STATUS_ERROR && error ? (
    <Error error={error} />
  ) : (
    <div>404</div>
  );
};

CardListComponent.propTypes = {
  currentCharacters: PropTypes.arrayOf(PropTypes.object).isRequired,
  status: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  currentCharacters: currentCharacters(state.character),
  status: state.character.status,
  error: state.character.error,
});

// Connect basic component to Redux store
const CardList = connect(mapStateToProps)(CardListComponent);

export default CardList;

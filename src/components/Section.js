import React from "react";
import styled from "styled-components";

const StyledSection = styled.section`
  margin: 0 42px;

  @media (max-width: 900px) {
    margin: 0 70px;
  }

  @media (max-width: 768px) {
    margin: 0 15px;
  }
`;

const Section = ({ children }) => {
  return <StyledSection>{children}</StyledSection>;
};

export default Section;

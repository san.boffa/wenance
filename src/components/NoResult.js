import React from "react";
import styled from "styled-components";

const StyledPage = styled.div`
  display: flex;
  height: 500px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: #fff;
  border-radius: 42px;
`;

const Loading = () => {
  return (
    <StyledPage>
      <h1>No results</h1>
    </StyledPage>
  );
};

export default Loading;

import React from "react";
import styled from "styled-components";
import UserInfo from "./UserInfo";

const StyledNav = styled.header`
  display: fixed;
  position: fixed;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 80px;
  top: 0;
  z-index: 999;

  background: #000000;

  @media (max-width: 720px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100px;
  }
  @media (max-width: 350px) {
    height: 150px;
  }
`;

const StyledLogo = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
`;

const StyledLogoImg = styled.img`
  margin: 0 10px 0 42px;

  @media (max-width: 720px) {
    margin: 0;
  }
`;

const StyledLogoText = styled.span`
  color: #fff;
  line-height: 34px;
  font-family: Cairo, Sans-serif;
  font-size: 18px;

  @media (max-width: 350px) {
    flex-direction: column;
    margin-top: 15px;
  }
`;

const Nav = () => {
  return (
    <StyledNav>
      <StyledLogo>
        <StyledLogoImg src="/logo.png" alt="Wenance" />
        <StyledLogoText>Code Challenge</StyledLogoText>
      </StyledLogo>
      <UserInfo />
    </StyledNav>
  );
};

export default Nav;

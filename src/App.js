import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchCharacter } from "./slice/character.slice";

import Home from "./page/Home";
import "./App.css";

const AppComponent = ({ fetchCharacter }) => {
  useEffect(() => {
    fetchCharacter("");
  }, []);

  return (
    <div className="App">
      <Home />
    </div>
  );
};

AppComponent.propTypes = {
  fetchCharacter: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  status: state.character.status,
  error: state.character.error,
});

const mapDispatchToProps = {
  fetchCharacter: fetchCharacter,
};

// Connect basic component to Redux store
const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent);

export default App;

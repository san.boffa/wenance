import React from "react";

import Container from "../components/Container";
import Section from "../components/Section";
import Header from "../components/Header";
import Nav from "../components/Nav";
import SearchForm from "../components/SearchForm";
import CardList from "../components/CardList";

const Home = () => {
  return (
    <>
      <Nav />
      <Container>
        <Header title="Character List" />
        <Section>
          <SearchForm />
          <CardList />
        </Section>
      </Container>
    </>
  );
};

export default Home;
